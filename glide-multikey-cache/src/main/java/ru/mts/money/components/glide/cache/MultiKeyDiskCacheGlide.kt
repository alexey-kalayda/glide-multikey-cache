package ru.mts.money.components.glide.cache

import android.content.Context
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import androidx.annotation.RawRes
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.cache.DiskCache

open class MultiKeyDiskCacheGlide(private val key: String) {

    var cache: DiskCache? = null

    fun with(view: View) = GlideInstance(view.context)

    fun with(context: Context) = GlideInstance(context)

    inner class GlideInstance(private val context: Context) {

        fun load(url: String) = Glide
            .with(context)
            .load(url)
            .signature()

        fun load(@RawRes @DrawableRes @Nullable resourceId: Int) = Glide
            .with(context)
            .load(resourceId)
            .signature()

        fun asFile() = Glide
            .with(context)
            .asFile()
            .signature()

        private fun <T> RequestBuilder<T>.signature() =
            signature(object : MultiKeyDiskCacheGlideKey(context, key) {
                override fun build() = super.build().also { cache = it }
            })
    }
}