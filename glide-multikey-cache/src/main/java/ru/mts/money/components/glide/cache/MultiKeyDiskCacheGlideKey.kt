package ru.mts.money.components.glide.cache

import android.content.Context
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.cache.DiskCache
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import java.security.MessageDigest

open class MultiKeyDiskCacheGlideKey(
    private val context: Context,
    val key: String
) : Key, DiskCache.Factory {

    final override fun updateDiskCacheKey(messageDigest: MessageDigest) = Unit

    override fun build() = InternalCacheDiskCacheFactory(
        context,
        key,
        DiskCache.Factory.DEFAULT_DISK_CACHE_SIZE.toLong()
    ).build()
}