package ru.mts.money.components.glide.cache

import android.content.Context
import com.bumptech.glide.load.engine.cache.DiskCache
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import java.util.*

class MultiKeyDiskCacheFactory(private val context: Context) : DiskCache.Factory {

    private val factoryByCK = mutableMapOf<String, DiskCache.Factory>()

    fun add(ck: String, name: String? = null) {
        factoryByCK += ck to InternalCacheDiskCacheFactory(
            context,
            name ?: ck.toLowerCase(Locale.ROOT),
            DiskCache.Factory.DEFAULT_DISK_CACHE_SIZE.toLong()
        )
    }

    override fun build() =
        MultiKeyDiskCache(
            defaultCache = InternalCacheDiskCacheFactory(context).build() ?: throw Exception(),
            cacheByCK = factoryByCK.entries
                .mapNotNull { (ck, factory) ->
                    factory.build()?.let { ck to it }
                }
                .toMap()
                .toMutableMap()
        )
}