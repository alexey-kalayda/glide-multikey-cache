package ru.mts.money.components.glide.cache

import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.cache.DiskCache
import java.io.File

/**
 * Кеш для сохранения и очистки данных по строковому ключу.
 * Ключ передаётся в методе signature() при построении цепочки
 * Glide.with().load()..signature(MultiKeyDiskCacheGlideKey(context, "key"))...into()
 * При первом обращении к Glide с такой сигнатурой вызывается метод
 * MultiKeyDiskCacheGlideKey.build(), который создает отдельный кеш для "key".
 * Чтобы сохранить этот кеш и в дальнейшем можно было, например, вызываеть его метод clear(),
 * следует в signature передавать объект класса-наследника MultiKeyDiskCacheGlideKey
 * с переопределенным build(), где сохраняется создаваемый DiskCache
 * (см. реализацию в MultiKeyDiskCacheGlide).
 * При последующих обращениях к Glide с ключом типа MultiKeyDiskCacheGlideKey с тем же "key"
 * build() не вызывается, а используется уже созданный кеш.
 *
 * В коде лучше использовать класс MultiKeyDiskCacheGlide, в котором уже реализована
 * подстановка signature() в цепочку Glide и сохранение кеша для ключа (в поле cache).
 * API ровно такой же, как у Glide.
 * Прмер использования MultiKeyDiskCacheGlide см. в SampleModuleGlide.
 * Главное отличие MultiKeyDiskCacheGlide от Glide в том, что это не синглтон.
 * То есть обращаться надо не к SampleModuleGlide.with(), а
 * val glide = SampleModuleGlide(context, "key")
 * glide.with()...
 */
class MultiKeyDiskCache(
    private val defaultCache: DiskCache,
    private val cacheByCK: MutableMap<String, DiskCache>
) : DiskCache {

    override fun clear() {
        defaultCache.clear()
        cacheByCK.values.forEach { it.clear() }
    }

    fun clearDefault() {
        defaultCache.clear()
    }

    override fun put(key: Key?, writer: DiskCache.Writer?) {
        key().put(key, writer)
    }

    override fun get(key: Key?): File? {
        return key().get(key)
    }

    override fun delete(key: Key?) {
        key().delete(key)
    }

    private operator fun Key?.invoke() = this
        ?.let {
            try {
                this::class.java.getDeclaredField("signature")
            } catch (ignore: Exception) {
                null
            }
                ?.apply { isAccessible = true }
                ?.get(this) as? MultiKeyDiskCacheGlideKey
        }
        ?.let { signature ->
            cacheByCK[signature.key]
                ?: (signature as? DiskCache.Factory)
                    ?.build()
                    ?.also { cacheByCK += signature.key to it }
        }
        ?: defaultCache
}