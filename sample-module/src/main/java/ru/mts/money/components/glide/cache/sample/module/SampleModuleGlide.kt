package ru.mts.money.components.glide.cache.sample.module

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import ru.mts.money.components.glide.cache.MultiKeyDiskCacheGlide

class SampleModuleGlide(
    private val context: Context,
    private val key: String
) : MultiKeyDiskCacheGlide(key) {

    fun load(url: String, view: ImageView) = this
        .with(context)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)

    fun clear() {
        cache?.clear()
    }

    fun list() =
        Glide.getPhotoCacheDir(context, key)
            ?.listFiles()
            ?.filter { file -> !file.isDirectory }
}