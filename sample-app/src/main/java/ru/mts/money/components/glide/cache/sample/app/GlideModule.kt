package ru.mts.money.components.glide.cache.sample.app

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import ru.mts.money.components.glide.cache.MultiKeyDiskCacheFactory

@Suppress("unused")
@GlideModule
class GlideModule : AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setDiskCache(MultiKeyDiskCacheFactory(context))
    }
}