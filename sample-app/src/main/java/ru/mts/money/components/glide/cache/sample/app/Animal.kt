package ru.mts.money.components.glide.cache.sample.app

data class Animal(
    val name: String?,
    val images: List<String>,
    var shown: Int = 0,
    var files: Int = 0,
    var scrollPosition: Int = 0
)