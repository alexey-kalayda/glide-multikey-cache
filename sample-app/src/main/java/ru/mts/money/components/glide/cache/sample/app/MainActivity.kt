package ru.mts.money.components.glide.cache.sample.app

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        root.adapter = AnimalsAdapter(animals.map { Animal(it.key, it.value) })
    }

    companion object {

        private val animals = mapOf(
            "cat" to listOf(
                "http://pluspng.com/img-png/cat-png-cat-png-clipart-1511.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Cat-PNG-5.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Cat-PNG-6.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Cat-PNG-14.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Cat-PNG.png"
            ),
            "dog" to listOf(
                "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-5.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-10.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-8.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-11.png",
                "http://www.pngall.com/wp-content/uploads/2016/03/Dog-PNG-6.png"
            ),
            null to listOf(
                "http://www.pngall.com/wp-content/uploads/2016/04/Mango-Download-PNG.png",
                "http://www.pngall.com/wp-content/uploads/2016/04/Banana-Free-Download-PNG.png",
                "http://www.pngall.com/wp-content/uploads/2016/05/Cherry-PNG-Clipart.png",
                "http://www.pngall.com/wp-content/uploads/2016/05/Pineapple-Free-Download-PNG.png",
                "http://www.pngall.com/wp-content/uploads/2016/04/Raspberry-Download-PNG.png"
            )
        )
    }
}