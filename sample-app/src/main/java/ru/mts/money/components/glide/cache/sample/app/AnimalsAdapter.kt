package ru.mts.money.components.glide.cache.sample.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.animal_images_layout.view.*

class AnimalsAdapter(
    private val animals: List<Animal>
) : RecyclerView.Adapter<AnimalsAdapter.ViewHolder>() {

    override fun getItemCount() = animals.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.animal_images_layout, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(animals[position])

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var animal: Animal? = null

        init {
            itemView.count.setOnClickListener {
                animal?.let {
                    it.files = (itemView.images.adapter as AnimalImagesAdapter).count()
                    itemView.count.text = it.files.toString()
                }
            }
            itemView.add.setOnClickListener {
                animal?.let {
                    it.shown++
                    itemView.images.adapter?.notifyItemInserted(it.shown - 1)
                    itemView.add.isEnabled = it.shown < it.images.size
                    it.files = (itemView.images.adapter as AnimalImagesAdapter).count()
                    itemView.count.text = it.files.toString()
                }
            }
            itemView.clear.setOnClickListener {
                animal?.let {
                    it.shown = 0
                    itemView.images.adapter?.notifyDataSetChanged()
                    itemView.add.isEnabled = true
                    (itemView.images.adapter as AnimalImagesAdapter).clear()
                    it.files = (itemView.images.adapter as AnimalImagesAdapter).count()
                    itemView.count.text = it.files.toString()
                }
            }
        }

        fun bind(animal: Animal) {
            this.animal = animal
            itemView.images.adapter =
                AnimalImagesAdapter(itemView.context, animal)
            itemView.images.scrollToPosition(animal.scrollPosition)
            itemView.count.text = animal.files.toString()
            itemView.add.isEnabled = animal.shown < animal.images.size
        }
    }
}