package ru.mts.money.components.glide.cache.sample.app

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.animal_image.view.*
import ru.mts.money.components.glide.cache.sample.module.SampleModuleGlide

class AnimalImagesAdapter(
    private val context: Context,
    private val animal: Animal
) : RecyclerView.Adapter<AnimalImagesAdapter.ViewHolder>() {

    private val glide = animal.name?.let { SampleModuleGlide(context, it) }

    override fun getItemCount() = animal.shown

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.animal_image, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(animal.images[position])

    fun clear() = glide?.clear() ?: Thread { Glide.get(context).clearDiskCache() }.start()

    fun count() = glide?.list()?.size ?: 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var url: String? = null

        fun bind(url: String) {
            this.url = url
            glide?.load(url, itemView.image) ?: Glide.with(context).load(url).into(itemView.image)
        }
    }
}